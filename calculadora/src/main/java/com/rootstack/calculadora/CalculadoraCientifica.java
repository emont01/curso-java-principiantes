/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.calculadora;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class CalculadoraCientifica extends Calculadora {

    public double raizCuadrada(double number) {
        return Math.sqrt(number);
    }

    public double potencia(double base, int exponente) {
        if (Math.abs(base) > 1000) {
            return 0;
        }
        if (Math.abs(base) < 0) {
            return 0;
        }
        return Math.pow(base, exponente);
    }
}
