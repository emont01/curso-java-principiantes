/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.calculadora;

import java.math.BigDecimal;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Calculadora extends Object {

    public Calculadora() {
    }

    public int sum(int a, int b) throws InvalidArgumentException {
        if (a == Integer.MAX_VALUE) {
            throw new InvalidArgumentException("el valor a es demasiado grande");
        }
        if (b == Integer.MAX_VALUE) {
            throw new InvalidArgumentException("el valor b es demasiado grande");
        }
        return a + b;
    }

    public double sum(double a, double b) {
        return a + b;
    }

    public float sum(float a, float b) {
        return a + b;
    }

    public BigDecimal sum(BigDecimal a, BigDecimal b) throws InvalidArgumentException  {
        if (a == null) {
            throw new InvalidArgumentException("el valor a no puede ser nulo");
        }
        if (b == null) {
            throw new InvalidArgumentException("el valor b no puede ser nulo");
        }
        return a.add(b);
    }

    public BigDecimal multiplicar(BigDecimal a, BigDecimal b) throws InvalidArgumentException {
        if (a == null) {
            throw new InvalidArgumentException("el valor a no puede ser nulo");
        }
        if (b == null) {
            throw new InvalidArgumentException("el valor b no puede ser nulo");
        }
        return a.multiply(b);
    }

    public double multiplicar(double a, double b) {
        return a * b;
    }

    int dividir(int a, int b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El valor b es cero");
        }
        return a / b;
    }

    float dividir(float a, float b) throws ArithmeticException, Exception {
        return a / b;
    }

    double dividir(double a, double b) throws InvalidArgumentException {
        if (b == 0) {
            throw new InvalidArgumentException("El valor b es cero");
        }
        return a / b;
    }

    BigDecimal dividir(BigDecimal a, BigDecimal b) throws ArithmeticException {
        return a.divide(b);
    }
}
