/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.calculadora;

import java.math.BigDecimal;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Main {

    public static void main(String[] args) throws InvalidArgumentException {
        Calculadora calc = crearCalculadora();

        if (calc instanceof CalculadoraCientifica) {
            CalculadoraCientifica cc = (CalculadoraCientifica) calc;
            double r = cc.raizCuadrada(25);
            System.out.println("Raiz cuadrada: " + r);

            r = cc.potencia(2, 3);
            System.out.println("2 al 3ra potencia: " + r);
        }

        /*doubleVsBigDecimal();
         dividirPorCero();
         dividir2();*/
    }

    private static Calculadora crearCalculadora() throws InvalidArgumentException {
        Calculadora calc = new CalculadoraCientifica();
        System.out.println("1 + 1: " + calc.sum(1, 1));
        System.out.println("1 + 1.0: " + calc.sum(1, 1.0));
        System.out.println("1 + 1.0f: " + calc.sum(1, 1.0f));
        return calc;
    }

    private static void doubleVsBigDecimal() throws InvalidArgumentException {
        double doubleSum = 0, oneCentDouble = 0.01;
        BigDecimal bigDecSum = BigDecimal.ZERO, oneCentBigDec = new BigDecimal("0.01");
        Calculadora calc = new Calculadora();
        for (int i = 1, len = 6; i <= len; i++) {
            doubleSum = calc.sum(oneCentDouble, doubleSum);
            bigDecSum = calc.sum(oneCentBigDec, bigDecSum);
        }
        System.out.println("La suma de double es: " + calc.multiplicar(oneCentDouble, 6));

        System.out.println("La suma de BigDecimal es: " + calc.multiplicar(oneCentBigDec, new BigDecimal("6")));
    }

    private static void dividirPorCero() {
        Calculadora calc = new Calculadora();

        int a = 100, b = 0;
        try {
            int resultado = calc.dividir(a, Integer.MAX_VALUE);
            System.out.println(resultado); // a/b
        } catch (ArithmeticException ex) {
            System.out.println("Ocurrió un error: " + ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        } finally {
            System.out.println("Se calculó la división");
        }
    }

    private static void dividir2() {
        Calculadora calc = new Calculadora();
        try {
            //calc.dividir(null, BigDecimal.ZERO);
            calc.dividir(BigDecimal.ONE, null);
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }
}
