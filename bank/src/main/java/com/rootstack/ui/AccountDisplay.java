/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.ui;

import com.rootstack.bank.Account;
import java.io.PrintStream;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class AccountDisplay {

    private final PrintStream out;

    public AccountDisplay(PrintStream out) {
        this.out = out;
    }

    public void show(Account account) {
        if (account == null) {
            return;
        }
        out.println("Cuenta: \t\t" + account.getAccountNumber());
        out.println("Dueño: \t\t\t" + account.getOwner().getName());
        out.println("Direccion del dueño: \t" + account.getOwner().getAddress().internatinalFormat());
        out.println("El balance es:  \t" + account.getBalance());
        out.println("\n\n");
    }

    public void show(Account[] accounts) {
        out.println("Cuenta\t\tBalance\t\tDueño");
        for (Account account : accounts) {
            out.printf(
                    "%s\t\t%s\t\t%s\n", 
                    account.getAccountNumber(), 
                    account.getBalance(), 
                    account.getOwner().getName()
            );
        }
    }

}
