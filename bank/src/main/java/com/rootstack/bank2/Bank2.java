/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.bank2;

import com.rootstack.bank.Account2;
import java.math.BigDecimal;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Bank2 {
    public static void main(String[] args) {
        Account2 acc2 = new Account2();
        acc2.addresss = "bla bla";
        acc2.owner = "Juan Perez";
        acc2.balance = new BigDecimal("1222");
        acc2.balance = acc2.balance.add(BigDecimal.ZERO);
        acc2.number = 1;
        
        System.out.println("Cuenta: \t\t" + acc2.number);
        System.out.println("Dueño: \t\t\t" + acc2.owner);
        System.out.println("Direccion del dueño: \t" + acc2.addresss);
        System.out.println("El balance es:  \t" + acc2.balance);
        System.out.println("\n\n");
        
        System.out.println(acc2.number + " " + acc2.balance + " " + acc2.owner);
    }
}
