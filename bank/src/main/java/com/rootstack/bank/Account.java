/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.bank;

import java.math.BigDecimal;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Account {
    private final int accountNumber;
    private final Person owner;
    private final BigDecimal balance;

    Account(int accountNumber, Person owner, String balance) {
        this.accountNumber = accountNumber;
        this.owner = owner;
        this.balance = new BigDecimal(balance);
    }

    public int getAccountNumber() {
        return accountNumber;
    }

    public Person getOwner() {
        return owner;
    }

    public BigDecimal getBalance() {
        return balance;
    }
    
}
