/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.bank;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Address {
    private final String[] addressData;

    public Address(String street, String location, String city, String country) {
        this.addressData = new String[] {
            street,
            location,
            city,
            country
        };
    }
    
    public String internatinalFormat() {
        return StringUtils.join(addressData, ", ");
    }
}
