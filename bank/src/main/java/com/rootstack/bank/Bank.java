/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.bank;

import com.rootstack.ui.AccountDisplay;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Bank {
    public static void main(String[] args) {
        Person tomas = new Person("Tomas");
        tomas.setAddress(new Address("casa 1, main street", "Betania", "Panama", "Rep. de Panama"));
        
        Person antonio = new Person("Antonio");
        antonio.setAddress(new Address("casa 2, main street", "Betania", "Panama", "Rep. de Panama"));
        
        Person sydney = new Person("Sydney");
        sydney.setAddress(new Address("casa 3, main street", "Betania", "Panama", "Rep. de Panama"));
        
        Person juan = new Person("Juan");
        juan.setAddress(new Address("casa 4, main street", "Betania", "Panama", "Rep. de Panama"));
        
        Account accounts[] = new Account[] {
            new Account(1, tomas, "1000"),
            new Account(2, antonio, "500"),
            new Account(3, sydney, "200"),
            new Account(4, juan, "450")
        };
        
        AccountDisplay display = new AccountDisplay(System.out);
        for(Account account : accounts) {
            display.show(account);
        }
        display.show(accounts);
    }
}
