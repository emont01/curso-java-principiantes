### ¿Para qué es este repositorio? ###

Este repo guarda el código fuente demostrado durante el curso de java para principiantes

### Referencias importantes ###
Los siguientes son URLs de referencia para continuar leyendo y aprender más detalles sobre temas tratados durante el curso:

* [Operadores en java](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html)
* [Tutorial en oracle](https://docs.oracle.com/javase/tutorial/)
* [Aprender java online](http://www.learnjavaonline.org/)
* [Tutorial de java](http://tutorials.jenkov.com/java/index.html)
* [Java pra principiantes](http://www.javabeginner.com/)
* [Tutorial en tutorials point](http://www.tutorialspoint.com/java/)
* [Tutorial de java en java2s](http://www.java2s.com/Tutorial/Java/CatalogJava.htm)
* [Tutorial de javatpoint](http://www.javatpoint.com/java-tutorial)
* [Tutorial en java begginers](http://javabeginnerstutorial.com/core-java/)
* [Java en homeandlearn](http://www.homeandlearn.co.uk/java/java.html)
