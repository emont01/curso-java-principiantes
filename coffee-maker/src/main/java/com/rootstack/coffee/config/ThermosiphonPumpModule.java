/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.config;

import com.rootstack.coffee.Pump;
import com.rootstack.coffee.Thermosiphon;
import dagger.Module;
import  dagger.Provides;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
@Module(complete = false, library = true)
class ThermosiphonPumpModule {

    @Provides
    Pump providePump(Thermosiphon pump) {
        return pump;
    }
}
