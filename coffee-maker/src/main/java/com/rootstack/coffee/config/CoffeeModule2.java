/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.config;

import com.rootstack.coffee.StandardCoffeeFilter;
import com.rootstack.coffee.Filter;
import com.rootstack.coffee.Grinder;
import com.rootstack.coffee.Heater;
import com.rootstack.coffee.CoffeeAppWithExceptionHandling;
import com.rootstack.coffee.ElectricGrinderWithExceptions;
import com.rootstack.coffee.ElectricHeaterWithExceptions;
import com.rootstack.coffee.MechanicalPump;
import com.rootstack.coffee.Pump;
import com.rootstack.coffee.ui.CliUserInterface;
import com.rootstack.coffee.ui.UserInterface;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * We need to explicitly register CoffeeApp as an injected type in the @Module annotation.
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
@Module(
    injects = CoffeeAppWithExceptionHandling.class
)
public class CoffeeModule2 {

    @Provides
    UserInterface provideUserInterface() {
        return new CliUserInterface();
    }

    /**
     * use our own instance creation
     *
     * @return
     */
    @Provides
    @Singleton
    Heater provideHeater(UserInterface ui) {
        return new ElectricHeaterWithExceptions(ui);
    }

    @Provides
    Filter provideFilter(StandardCoffeeFilter filter) {
        return filter;
    }

    /**
     * Allows dager to create the ElectricGrinder instance
     *
     * @param grinder
     * @return
     */
    @Provides
    Grinder provideGrinder(ElectricGrinderWithExceptions grinder) {
        return grinder;
    }
    
    @Provides
    Pump providePump(MechanicalPump pump) {
        return pump;
    }
}
