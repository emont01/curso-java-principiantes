/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.config;

import com.rootstack.coffee.StandardCoffeeFilter;
import com.rootstack.coffee.ElectricGrinder;
import com.rootstack.coffee.ElectricHeater;
import com.rootstack.coffee.Filter;
import com.rootstack.coffee.Grinder;
import com.rootstack.coffee.Heater;
import com.rootstack.coffee.CoffeeApp;
import com.rootstack.coffee.ui.CliUserInterface;
import com.rootstack.coffee.ui.UserInterface;
import dagger.Module;
import dagger.Provides;
import javax.inject.Singleton;

/**
 * We need to explicitly register CoffeeApp as an injected type in the @Module annotation.
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
@Module(
    injects = CoffeeApp.class,
    includes = ThermosiphonPumpModule.class
)
public class CoffeeModule {

    @Provides
    UserInterface provideUserInterface() {
        return new CliUserInterface();
    }

    /**
     * use our own instance creation
     *
     * @return
     */
    @Provides
    @Singleton
    Heater provideHeater(UserInterface ui) {
        return new ElectricHeater(ui);
    }

    @Provides
    Filter provideFilter(StandardCoffeeFilter filter) {
        return filter;
    }

    /**
     * Allows dager to create the ElectricGrinder instance
     *
     * @param grinder
     * @return
     */
    @Provides
    Grinder provideGrinder(ElectricGrinder grinder) {
        return grinder;
    }
}
