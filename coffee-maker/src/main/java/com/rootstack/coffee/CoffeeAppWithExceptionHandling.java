/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
import com.rootstack.coffee.exceptions.HeaterStateException;
import com.rootstack.coffee.exceptions.PumpException;
import com.rootstack.coffee.makers.BigCoffeeMaker;
import com.rootstack.coffee.makers.GrindingCoffeeMaker;
import com.rootstack.coffee.makers.SimpleCoffeeMaker;
import com.rootstack.coffee.ui.UserInterface;
import javax.inject.Inject;

public class CoffeeAppWithExceptionHandling implements Runnable {

    @Inject
    SimpleCoffeeMaker coffeeMaker;
    @Inject
    BigCoffeeMaker bigCoffeeMaker;
    @Inject
    GrindingCoffeeMaker grindingCoffeeMaker;
    @Inject
    UserInterface ui;

    @Override
    public void run() {
        try {
            coffeeMaker.brew();
            bigCoffeeMaker.brew(5);
            grindingCoffeeMaker.brew(5);
        } catch (PumpException | HeaterStateException ex) {
            ui.message(ex.getMessage(), this);
        } catch(Exception ex) {
            ui.message(ex.getMessage(), this);
            ex.printStackTrace(System.out);
        }
    }
}
