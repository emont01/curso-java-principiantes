/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.makers;

import com.rootstack.coffee.Heater;
import com.rootstack.coffee.Pump;
import com.rootstack.coffee.ui.UserInterface;
import dagger.Lazy;
import javax.inject.Inject;

/**
 * This is a self service coffee maker, it only brew the coffee for you
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class SimpleCoffeeMaker {

    @Inject Lazy<Heater> heater; // Don't want to create a possibly costly heater until we need it.
    @Inject Pump pump;
    @Inject UserInterface ui;

    public void brew() {
        heater.get().on();
        pump.pump();
        notifyReady(); // let the user know that the coffee is ready
        heater.get().off();
    }
    
    public void notifyReady()
    {
        ui.message("[_]P coffee! [_]P", this);
    }
}
