/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.makers;

import com.rootstack.coffee.Filter;
import com.rootstack.coffee.Grinder;
import com.rootstack.coffee.Heater;
import com.rootstack.coffee.Pump;
import com.rootstack.coffee.exceptions.GrinderException;
import com.rootstack.coffee.ui.UserInterface;
import dagger.Lazy;
import java.util.Random;
import javax.inject.Inject;
import javax.inject.Provider;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class GrindingCoffeeMaker {

    @Inject
    Lazy<Grinder> grinder;
    @Inject
    Provider<Filter> filterProvider;
    @Inject
    Lazy<Heater> heater; // Don't want to create a possibly costly heater until we need it.
    @Inject
    Pump pump;
    @Inject
    UserInterface ui;
    
    private Filter filter;

    public void brew() throws GrinderException {
        while (needsGrinding()) {
            // Grinder created once on first call to .get() and cached.
            grinder.get().grind();
        }
    }

    public void brew(int numberOfPots) throws GrinderException {
        heater.get().on();
        for (int p = 0; p < numberOfPots; p++) {
            setFilter(filterProvider.get()); //new filter every time.
            if (needsGrinding()) {
                grinder.get().grind();
            }
            percolate();
            notifyReady(); // will wait until the next pot is on
        }
        heater.get().off();
    }

    /**
     * replace the coffee filter with the new one sent
     *
     * @param filter
     */
    private void setFilter(Filter filter) {
        this.filter = filter;
        this.filter.install();
    }

    /**
     * let's assume that pumping water is enough to percolate it using the current installed filer.
     */
    private void percolate() {
        pump.pump();
    }

    /**
     * simulates a blocking IO call
     */
    private void notifyReady() {
        try {
            ui.message("[_]P coffee! [_]P", this);
            Random rand = new Random();
            Thread.sleep(rand.nextInt(4000));
        } catch (InterruptedException ex) {
            // just ignore this
        }
    }

    private boolean needsGrinding() {
        Random rand = new Random();
        return rand.nextInt(100) > 50;
    }
}
