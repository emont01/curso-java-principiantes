/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.config.CoffeeModule;
import com.rootstack.coffee.config.CoffeeModule2;
import dagger.ObjectGraph;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Main {
    public static void main(String[] args) {
        ObjectGraph obj;
                
        obj = ObjectGraph.create(CoffeeModule.class);
        CoffeeApp app1 = obj.get(CoffeeApp.class);
        //app1.run();
        
        
        obj = ObjectGraph.create(CoffeeModule2.class);
        CoffeeAppWithExceptionHandling app2 = obj.get(CoffeeAppWithExceptionHandling.class);
        app2.run();
    }
}
