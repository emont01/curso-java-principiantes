/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
import com.rootstack.coffee.makers.BigCoffeeMaker;
import com.rootstack.coffee.makers.GrindingCoffeeMaker;
import com.rootstack.coffee.makers.SimpleCoffeeMaker;
import javax.inject.Inject;

public class CoffeeApp implements Runnable {

    @Inject
    SimpleCoffeeMaker coffeeMaker;
    @Inject
    BigCoffeeMaker bigCoffeeMaker;
    @Inject
    GrindingCoffeeMaker grindingCoffeeMaker;

    @Override
    public void run() {
        coffeeMaker.brew();
        bigCoffeeMaker.brew(5);
    }
}
