/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class Operators {

    public static void main(String[] args) {
        assign();
        arithmetic();
        relational();
        bitwise1();
        bitwise2();
        compound();
        ternary();
    }

    private static void assign() {
        //Assigning Primitive Values
        int j, k;
        j = 10; // j gets the value 10.
        j = 5; // j gets the value 5. Previous value is overwritten.
        k = j; // k gets the value 5.
        System.out.println("j is : " + j);
        System.out.println("k is : " + k);
        //	        Assigning References
        Integer i1 = new Integer("1");
        Integer i2 = new Integer("2");
        System.out.println("i1 is : " + i1);
        System.out.println("i2 is : " + i2);

        i1 = i2;
        System.out.println("i1 is : " + i1);
        System.out.println("i2 is : " + i2);

        //Multiple Assignments
        k = j = 10; // (k = (j = 10))
        System.out.println("j is : " + j);
        System.out.println("k is : " + k);
    }

    private static void arithmetic() {
        int x, y = 10, z = 5;
        x = y + z;
        System.out.println("+ operator resulted in " + x);
        x = y - z;
        System.out.println("- operator resulted in " + x);
        x = y * z;
        System.out.println("* operator resulted in " + x);
        x = y / z;
        System.out.println("/ operator resulted in " + x);
        x = y % z;
        System.out.println("% operator resulted in " + x);
        x = y++;
        System.out.println("Postfix ++ operator resulted in " + x);
        x = ++z;
        System.out.println("Prefix ++ operator resulted in " + x);
        x = -y;
        System.out.println("Unary operator resulted in " + x);

        // Some examples of special Cases
        int tooBig = Integer.MAX_VALUE + 1; // -2147483648 which is Integer.MIN_VALUE.
        int tooSmall = Integer.MIN_VALUE - 1; // 2147483647 which is Integer.MAX_VALUE.

        System.out.println("tooBig becomes " + tooBig);
        System.out.println("tooSmall becomes " + tooSmall);
        System.out.println(4.0 / 0.0); // Prints: Infinity
        System.out.println(-4.0 / 0.0); // Prints: -Infinity
        System.out.println(0.0 / 0.0); // Prints: NaN

        double d1 = 12 / 8; // result: 1 by integer division. d1 gets the value 1.0.
        double d2 = 12.0F / 8; // result: 1.5
        System.out.println("d1 is " + d1);
        System.out.println("d2 iss " + d2);
    }

    private static void relational() {
        int x = 10, y = 5;
        System.out.println("x > y : " + (x > y));
        System.out.println("x < y : " + (x < y));
        System.out.println("x >= y : " + (x >= y));
        System.out.println("x <= y : " + (x <= y));
        System.out.println("x == y : " + (x == y));
        System.out.println("x != y : " + (x != y));
    }

    private static void logical() {
        boolean x = true;
        boolean y = false;
        System.out.println("x & y : " + (x & y));
        System.out.println("x && y : " + (x && y));
        System.out.println("x | y : " + (x | y));
        System.out.println("x || y: " + (x || y));
        System.out.println("x ^ y : " + (x ^ y));
        System.out.println("!x : " + (!x));
    }

    private static void bitwise1() {
        int x = 0xFAEF; //1 1 1 1 1 0 1 0 1 1 1 0 1 1 1 1 
        int y = 0xF8E9; //1 1 1 1 1 0 0 0 1 1 1 0 1 0 0 1 
        int z;
        System.out.println("x & y : " + (x & y));
        System.out.println("x | y : " + (x | y));
        System.out.println("x ^ y : " + (x ^ y));
        System.out.println("~x : " + (~x));
        System.out.println("x << y : " + (x << y));
        System.out.println("x >> y : " + (x >> y));
        System.out.println("x >>> y : " + (x >>> y));
        //There is no unsigned left shift operator
    }

    /*
     * The below program demonstrates bitwise operators keeping in mind operator precedence
     * Operator Precedence starting with the highest is -> |, ^, &
     */
    private static void bitwise2() {
        int a = 1 | 2 ^ 3 & 5;
        int b = ((1 | 2) ^ 3) & 5;
        int c = 1 | (2 ^ (3 & 5));
        System.out.print(a + "," + b + "," + c);
    }

    public static void compound() {
        int x = 0, y = 5;
        x += 3;
        System.out.println("x : " + x);
        y *= x;
        System.out.println("y :  " + y);
    }

    private static void ternary() {

        int x = 10, y = 12, z = 0;
        z = x > y ? x : y;
        System.out.println("z : " + z);

        boolean t1 = false ? false : true ? false : true ? false : true;
        boolean t2 = false ? false
                : (true ? false : (true ? false : true));
        boolean t3 = ((false ? false : true) ? false : true) ? false
                : true;
        System.out.println(m1(t1) + m1(t2) + m1(t3));
    }

    static String m1(boolean b) {
        return b ? "T" : "F";
    }
}
