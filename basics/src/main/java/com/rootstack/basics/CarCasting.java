/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

import com.rootstack.basics.casting.*;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class CarCasting {
    public static void main(String[] args) {
        Car obj = new Ford();

	//Lo siguiente resulta en un error de compilación
        //obj.fordMethod();	//fordMethod no existe en Car
	
        //Lo siguiente resulta en un error de compilación
        //((HeavyVehicle)obj).fordMethod(); //fordMethod no existe en HeavyVehicle
        //Lo siguiente compilará y correrá correctamente
        ((Ford) obj).fordMethod();

        //Lo siguiente resulta en un error de compilación
        //Honda hondaObj = (Ford)obj; //Cannot convert as they are sibblings
    }
}
