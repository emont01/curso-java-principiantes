/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class JavaTypes {

    public static void main(String[] args) {
        primitives();
        nonPrimitives();
    }

    private static void primitives() {
        byte zero = 0;
        short a = 3;
        int b = 1;
        char d = ' ';
        float e = 2.0f;
        boolean f = true;
        String output = "H" + a + b + b + zero + d + "w" + zero + "r" + b + "d" + d + e + d + f;
        System.out.println(output);
        System.out.println("Suma es: " + (a + b));
    }

    private static void nonPrimitives() {
        Byte zero = 0;
        Short a = 3;
        Integer b = 1;
        Character d = ' ';
        Float e = 2.0f;
        Double f = 3.0;
        Boolean g = true;
        String output = "H" + a + b + b + zero + d + "w" + zero + "r" + b + "d" + d + e + d
                + f.doubleValue() + ' ' + g.booleanValue();
        System.out.println(output);
    }

}
