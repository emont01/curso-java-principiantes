/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

import com.rootstack.basics.classes.PublicClass;
import com.rootstack.basics.classes.PublicSubClass;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ClassesObjectsMethods {
    public static void main(String[] args) {
        // public class es accesible
        PublicClass p = new PublicClass();
        p.dummy();
        
        // public sub class es accesible
        PublicClass ps = PublicClass.buildInstance(1);
        ps.dummy();
        
        PublicClass p1 = PublicClass.buildInstance(2);
        p1.dummy();
        
        // default class no es visible, esto produce un error en tiempo de compilación.
        //DefaultClass d = new DefaultClass();
    }
}

