/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

import java.util.Vector;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class StringCasting {

    public static void main(String args[]) {
        String username = "asdf";
        String password = "qwer";
        Vector v = new Vector();
        v.add(username);
        v.add(password);
        //String u = v.elementAt(0); No se puede converitr de objeto a string
        Object u = v.elementAt(0);
        System.out.println("Username : " + u);
        String uname = (String) v.elementAt(0); // cast permitido
        String pass = (String) v.elementAt(1); // cast permitido
        System.out.println();
        System.out.println("Username : " + uname);
        System.out.println("Password : " + pass);
    }
}
