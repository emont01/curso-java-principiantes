/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.overloading;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ConstructorOverload {
    public static void main(String[] args) {
        System.out.println("Cube1: " + new Cube1().getVolume()); // 1000
        System.out.println("Cube1: " + new Cube1(2, 2, 2).getVolume()); // 8
        
        System.out.println("Cube1: " + new Cube2().getVolume()); //1000
        System.out.println("Cube1: " + new Cube2(2, 2).getVolume()); //40 (2 * 2 * 10)
        System.out.println("Cube1: " + new Cube2(2, 2, 2).getVolume()); //8
        System.out.println("Cube1: " + new Cube2(2).getVolume()); //8
    }
}

class Cube1 {

    int length;
    int breadth;
    int height;

    public int getVolume() {
        return (length * breadth * height);
    }

    Cube1() {
        length = 10;
        breadth = 10;
        height = 10;
    }

    Cube1(int l, int b, int h) {
        length = l;
        breadth = b;
        height = h;
    }
}

class Cube2 {

    int length;
    int breadth;
    int height;

    public int getVolume() {
        return (length * breadth * height);
    }

    Cube2() {
        this(10, 10);
        System.out.println("Finished with Default Constructor");
    }
    
    Cube2(int l) {
        this(l, 10, 10);
    }

    Cube2(int l, int b) {
        this(l, b, 10);
        System.out.println("Finished with Parameterized Constructor having 2 params");
    }

    Cube2(int l, int b, int h) {
        length = l;
        breadth = b;
        height = h;
        System.out.println("Finished with Parameterized Constructor having 3 params");
    }
}
