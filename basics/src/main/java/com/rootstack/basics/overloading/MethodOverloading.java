/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.overloading;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class MethodOverloading {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        byte ba = 2, bb = 2, bs;
        int ia = 100, ib = 200, is;
        double da = 1.5, db = 1.5, ds;
        bs = calc.sum(ba, bb);
        is = calc.sum(ia, ib);
        ds = calc.sum(da, db);
        
        System.out.println("short a + b is : " + bs);
        System.out.println("int a + b is : " + is);
        System.out.println("double a + b is : " + ds);
        
        is = Calculator.s(ia, ib);
        System.out.println("int a + b is : " + is);
        
        System.out.println("raiz cuadrada : " + Math.sqrt(is));
    }
}

class Calculator {
    public byte sum(byte a, byte b){
        return (byte)(a + b); //suma entera
    }
    public short sum(short a, short b){
        return (short)(a + b); //suma entera
    }
    public int sum(int a, int b){
        return a + b;
    }
    public long sum(long a, long b){
        return a + b;
    }
    public float sum(float a, float b){
        return a + b;
    }
    public double sum(double a, double b){
        return a + b;
    }
    
    public static int s(int a, int b) {
        return a + b;
    }
}
