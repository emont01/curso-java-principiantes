/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.overloading;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ConstructorChaining {

    public static void main(String[] args) {
        SpecialCube specialObj1 = new SpecialCube();
        SpecialCube specialObj2 = new SpecialCube(10, 20);

        System.out.println("Volume of SpecialCube1 is : " + specialObj1.getVolume());
        System.out.println("Weight of SpecialCube1 is : " + specialObj1.weight);
        System.out.println("Volume of SpecialCube2 is : " + specialObj2.getVolume());
        System.out.println("Weight of SpecialCube2 is : " + specialObj2.weight);
    }
}

class Cube {

    int length;
    int breadth;
    int height;

    public int getVolume() {
        return (length * breadth * height);
    }

    Cube() {
        this(10, 10);
        System.out.println("Finished with Default Constructor of Cube");
    }

    Cube(int l, int b) {
        this(l, b, 10);
        System.out.println("Finished with Parameterized Constructor having 2 params of Cube");
    }

    Cube(int l, int b, int h) {
        length = l;
        breadth = b;
        height = h;
        System.out.println(
                "Finished with Parameterized Constructor having 3 params of Cube");
    }
}

class SpecialCube extends Cube {

    int weight;

    SpecialCube() {
        super();
        weight = 10;
    }

    SpecialCube(int l, int b) {
        this(l, b, 10);
        System.out.println(
                "Finished with Parameterized Constructor having 2 params of SpecialCube");
    }

    SpecialCube(int l, int b, int h) {
        super(l, b, h);
        weight = 20;
        System.out.println("Finished with Parameterized Constructor having 3 params of SpecialCube ");
    }
}
