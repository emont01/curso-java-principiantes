/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

import com.rootstack.basics.casting.HeavyVehicle;
import com.rootstack.basics.casting.Truck;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class InstanceOf {

    static boolean result;
    static HeavyVehicle hV = new HeavyVehicle();
    static Truck T = new Truck();
    static HeavyVehicle hv2 = null;

    public static void main(String[] args) {
        result = hV instanceof HeavyVehicle;
        System.out.print("hV is an HeavyVehicle: " + result + "\n");
        
        result = T instanceof HeavyVehicle;
        System.out.print("T is an HeavyVehicle: " + result + "\n");
        
        result = hV instanceof Truck;
        System.out.print("hV is a Truck: " + result + "\n");
        
        result = hv2 instanceof HeavyVehicle;
        System.out.print("hv2 is an HeavyVehicle: " + result + "\n");
        
        hV = T; //Correct Cast de hijo a padre
        T = (Truck) hV; //Correcto cast explicito de padre a hijo
    }
}
