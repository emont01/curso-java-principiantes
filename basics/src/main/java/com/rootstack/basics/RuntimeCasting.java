/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
//X is a supper class of Y and Z which are sibblings.
class X {
}

class Y extends X {
}

class Z extends X {
}

public class RuntimeCasting {

    public static void main(String args[]) {
        X x = new X();
        Y y = new Y();
        Z z = new Z();
        X xy = new Y(); // compila ok (hacia arriba en la jerarquia)
        X xz = new Z(); // compila ok (hacia arriba en la jerarquia)
        //Y yz = new Z();   //tipo incompatible (hermanos)
        //Y y1 = new X();   //X no es Y
        //Z z1 = new X();   //X no es Z
        X x1 = y; // compila ok (y is hijo de X)
        X x2 = z; // compila ok (z is hijo de X)
        Y y1 = (Y) x; // compila ok pero produce error al ejecutar
        Z z1 = (Z) x; // compila ok pero produce error al ejecutar
        Y y2 = (Y) x1; // compila and corre ok (x1 is type Y)
        Z z2 = (Z) x2; // compila and corre ok (x2 is type Z)
        //Y y3 = (Y) z;     //tipo inconvertible (hermanos)
        //Z z3 = (Z) y;     //tipo inconvertible (hermanos)
        Object o = z;
        Object o1 = (Y) o; // compila ok pero produce error al ejecutar
    }
}
