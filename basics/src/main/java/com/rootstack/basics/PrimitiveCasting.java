/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class PrimitiveCasting {

    public static void main(String[] args) {
        widening();
        narowing();
    }

    private static void widening() {
        int big = 1234567890;
        float approx = big;
        System.out.println(big - (int) approx);
    }

    /**
     * 
     */
    private static void narowing() {
        float fmin = Float.NEGATIVE_INFINITY;
        float fmax = Float.POSITIVE_INFINITY;
        System.out.println("long: " + (long) fmin
                + ".." + (long) fmax);
        System.out.println("int: " + (int) fmin
                + ".." + (int) fmax);
        System.out.println("short: " + (short) fmin
                + ".." + (short) fmax);
        System.out.println("char: " + (int) (char) fmin
                + ".." + (int) (char) fmax);
        System.out.println("byte: " + (byte) fmin
                + ".." + (byte) fmax);
    }
}
