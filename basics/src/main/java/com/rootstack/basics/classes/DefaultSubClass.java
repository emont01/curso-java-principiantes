/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.classes;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
class DefaultSubClass extends PublicClass {
    public void dummy() {
        System.out.println("ON default public class");
    }
}
