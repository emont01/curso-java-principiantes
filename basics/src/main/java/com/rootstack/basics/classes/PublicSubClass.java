/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.classes;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class PublicSubClass extends PublicClass {

    private final DefaultClass defaultInstance;

    public PublicSubClass() {
        // sub clase puede acceder al valor protegido
        this.protectedValue = 3;
        // sub clase no puede acceder al valor privado
        //this.privateValue = 1;
        // la clase con accesibilidad default es visible
        defaultInstance = new DefaultClass();
    }
    
    public void dummy(){
        System.out.println("ON sub public class");
    }
}
