/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.classes;

/**
 * Access specifiers example
 * 1. private
 * 2. protected
 * 3. default
 * 4. public
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class PublicClass {
    private int privateValue = 1;
    protected int protectedValue = 2;
    private final ProtectedClass protectedInstance;
    private final PrivateClass privateInstance;
    private final DefaultClass defaultInstance;

    public static PublicClass buildInstance(int i) {
        if (i == 1) {
            return new DefaultSubClass();
        }
        return new PublicSubClass();
    }
    
    protected class ProtectedClass {}
    private class PrivateClass {}
    
    public PublicClass() {
        // esta clase tiene accesso a las variables protegidas
        protectedInstance = new ProtectedClass();
        protectedValue = 1;
        
        // esta clase tiene accesso a las variables privadas
        privateInstance = new PrivateClass();
        privateValue = 2;
        
        // la clase con accesibilidad default es visible
        defaultInstance = new DefaultClass();
    }
    
    public void dummy(){
        System.out.println("ON public class");
    }
}
