/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.basics.classes;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
class DefaultClass {

    private int i = 0;
    
    public int defaultClassPublic() {
        return i;
    }
    
    public static int defaultClassStatic() {
        return Integer.MIN_VALUE;
    }
}
