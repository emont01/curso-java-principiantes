/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public interface Pump {

    void pump();
}
