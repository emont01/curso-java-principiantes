/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.exceptions.HeaterStateException;
import com.rootstack.coffee.ui.UserInterface;
import java.util.Random;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ElectricHeaterWithExceptions implements Heater {

    boolean heating;
    private final UserInterface ui;
    
    @Inject
    public ElectricHeaterWithExceptions(UserInterface ui) {
        this.ui = ui;
    }
    
    @Override
    public void on() {
        ui.message("~ ~ ~ heating ~ ~ ~", this);
        this.heating = true;
    }

    @Override
    public void off() {
        this.heating = false;
    }

    @Override
    public boolean isHot() {
        Random rand = new Random();
        if (rand.nextInt(100) > 80) {
            throw new HeaterStateException("Heater malfunction resulted in an unknow state");
        }
        return heating;
    }
}
