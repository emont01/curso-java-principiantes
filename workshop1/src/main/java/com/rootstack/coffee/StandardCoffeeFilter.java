/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.ui.UserInterface;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class StandardCoffeeFilter implements Filter {

    private final UserInterface ui;

    @Inject
    public StandardCoffeeFilter(UserInterface ui) {
        this.ui = ui;
    }

    @Override
    public void install() {
        ui.message("Filter installed!", this);
    }

}
