/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.exceptions.GrinderException;
import com.rootstack.coffee.exceptions.HeaterStateException;
import com.rootstack.coffee.ui.UserInterface;
import java.util.Random;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ElectricGrinderWithExceptions implements Grinder {
    private final UserInterface ui;

    @Inject
    public ElectricGrinderWithExceptions(UserInterface ui) {
        this.ui = ui;
    }

    @Override
    public void grind() throws GrinderException {
        Random rand = new Random();
        if (rand.nextInt(100) > 70) {
            throw new GrinderException("The coffee is stuck in the grinder");
        }
        ui.message(". . . grinding . . .", this);
    }

}
