/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.ui.UserInterface;
import java.util.Random;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ElectricHeater implements Heater {

    boolean heating;
    private final UserInterface ui;
    
    @Inject
    public ElectricHeater(UserInterface ui) {
        this.ui = ui;
    }
    
    @Override
    public void on() {
        ui.message("~ ~ ~ heating ~ ~ ~", this);
        this.heating = true;
    }

    @Override
    public void off() {
        this.heating = false;
    }

    @Override
    public boolean isHot() {
        return heating;
    }
}
