/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.ui;

import java.time.LocalDateTime;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class CliUserInterface implements UserInterface {
    @Override
    public void message(String message)
    {
        message(message, "");
    }
    
    @Override
    public void message(String message, Object source)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(LocalDateTime.now()).append(" ");
        if (source instanceof String) {
            sb.append(source);
        } else {
            sb.append(source.getClass().getCanonicalName());
        }
        sb.append(": ").append(message);
        System.out.println(sb.toString());
    }
}
