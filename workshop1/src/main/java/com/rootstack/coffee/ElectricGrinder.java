/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.exceptions.GrinderException;
import com.rootstack.coffee.ui.UserInterface;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class ElectricGrinder implements Grinder {
    private final UserInterface ui;

    @Inject
    public ElectricGrinder(UserInterface ui) {
        this.ui = ui;
    }

    @Override
    public void grind() throws GrinderException {
        if (ui == null) {
            throw new GrinderException("This will never happen");
        }
        ui.message(". . . grinding . . .", this);
    }

}
