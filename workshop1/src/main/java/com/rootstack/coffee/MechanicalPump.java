/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.exceptions.PumpException;
import com.rootstack.coffee.exceptions.HeaterStateException;
import com.rootstack.coffee.ui.UserInterface;
import java.util.Random;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class MechanicalPump implements Pump {

    private final UserInterface ui;

    @Inject
    public MechanicalPump(UserInterface ui) {
        this.ui = ui;
    }

    @Override
    public void pump() {
        Random rand = new Random();
        if (rand.nextInt(100) > 50) {
            throw new PumpException("Power failure detected, pump malfunction");
        }
        ui.message("=> => pumping => =>", this);
    }

}
