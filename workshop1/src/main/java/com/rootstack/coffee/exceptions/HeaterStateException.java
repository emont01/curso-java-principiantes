/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.exceptions;

/**
 * Here's the bottom line guideline: If a client can reasonably be expected to recover from an exception, make it a 
 * checked exception. If a client cannot do anything to recover from the exception, make it an unchecked exception.
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class HeaterStateException extends RuntimeException {

    public HeaterStateException() {
    }

    public HeaterStateException(String message) {
        super(message);
    }

    public HeaterStateException(String message, Throwable cause) {
        super(message, cause);
    }

    public HeaterStateException(Throwable cause) {
        super(cause);
    }
    
}
