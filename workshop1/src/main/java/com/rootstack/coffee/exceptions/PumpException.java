/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.exceptions;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class PumpException extends RuntimeException {

    public PumpException() {
    }

    public PumpException(String message) {
        super(message);
    }

    public PumpException(String message, Throwable cause) {
        super(message, cause);
    }

    public PumpException(Throwable cause) {
        super(cause);
    }
    
}
