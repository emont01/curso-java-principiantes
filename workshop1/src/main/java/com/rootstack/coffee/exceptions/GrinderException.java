/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee.exceptions;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */
public class GrinderException extends Exception {

    public GrinderException() {
    }

    public GrinderException(String message) {
        super(message);
    }

    public GrinderException(String message, Throwable cause) {
        super(message, cause);
    }

    public GrinderException(Throwable cause) {
        super(cause);
    }
    
}
