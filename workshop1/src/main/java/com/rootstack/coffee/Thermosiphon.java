/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rootstack.coffee;

import com.rootstack.coffee.ui.UserInterface;
import javax.inject.Inject;

/**
 *
 * @author Eivar A. Montenegro M. <e.mont01 at gmail.com>
 */

public class Thermosiphon implements Pump {

    private final Heater heater;
    private final UserInterface ui;

    @Inject
    public Thermosiphon(Heater heater, UserInterface ui) {
        this.heater = heater;
        this.ui = ui;
    }

    @Override
    public void pump() {
        if (heater.isHot()) {
            ui.message("=> => pumping => =>", this);
        }
    }
}
